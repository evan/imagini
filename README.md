<p align="center">
    <img src="https://gitea.va.reichard.io/evan/imagini/raw/branch/master/assets/imagini_full.png" width="300">
</p>

---

A self hosted photo library with user management & authentication. Cross platform Client supporting Android, iOS, and Web.

## Client
See `web_native` [subfolder](./web_native/README.md)

## Server
### Features / Roadmap
- [DONE] DB w/ user management - bcrypt salt & hash
- [DONE] JWT Access & Refresh Tokens
- [DONE] GraphQL API
- [DONE] GraphQL multipart upload
- [DONE] GraphQL basic filtering, ordering, pagination
- [DONE] Uploading images - exif extraction (load db with lat, long, etc)
- [DONE] Dynamic image conversion (heif support, width params)

- [TODO] ALL the tests
- [TODO] GraphQL & DB deletes & update
- [TODO] Dockerfile
- [TODO] Resolving GraphQL nested queries (e.g. albums, tags)
- [TODO] GraphQL nested filters
- [TODO] Lots more... TBD

### Dependencies

- libvips 8.8+

### Running

    CONFIG_PATH=$(pwd) DATA_PATH=$(pwd) go run cmd/main.go serve

## Building

    # Generate GraphQL Models
    go run github.com/99designs/gqlgen generate
    go run cmd/main.go generate

    # Generate GraphQL Documentation
    graphdoc -e http://localhost:8484/query -o ./docs/schema
