package graph

import (
	"reichard.io/imagini/internal/auth"
	"reichard.io/imagini/internal/config"
	"reichard.io/imagini/internal/db"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	Config *config.Config
	Auth   *auth.AuthManager
	DB     *db.DBManager
}
