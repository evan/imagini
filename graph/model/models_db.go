package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	newID := uuid.New().String()
	u.ID = newID
	return
}

func (a *Album) BeforeCreate(tx *gorm.DB) (err error) {
	newID := uuid.New().String()
	a.ID = newID
	return
}

func (m *MediaItem) BeforeCreate(tx *gorm.DB) (err error) {
	newID := uuid.New().String()
	m.ID = newID
	return
}

func (t *Tag) BeforeCreate(tx *gorm.DB) (err error) {
	newID := uuid.New().String()
	t.ID = newID
	return
}

func (d *Device) BeforeCreate(tx *gorm.DB) (err error) {
	newID := uuid.New().String()
	d.ID = newID
	return
}
