package model

import (
	"net/http"

	"github.com/lestrrat-go/jwx/jwt"
)

type AuthContext struct {
	AccessToken  *jwt.Token
	AuthResponse *http.ResponseWriter
	AuthRequest  *http.Request
}
