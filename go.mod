module reichard.io/imagini

go 1.15

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/gabriel-vasile/mimetype v1.1.2
	github.com/google/uuid v1.1.5
	github.com/h2non/bimg v1.1.5
	github.com/iancoleman/strcase v0.1.3
	github.com/lestrrat-go/jwx v1.0.8
	github.com/sirupsen/logrus v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	github.com/vektah/gqlparser/v2 v2.1.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.20.9
)
