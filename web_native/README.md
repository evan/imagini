# Imagini Client
A cross platform (iOS, Android, & Web) client used with the Imagini server.

## Features / Roadmap
- [DONE] Login w/ errors
- [DONE] GraphQL client
- [DONE] Load & tile images for user
- [DONE] Access & Refresh Token secure storage (`localStorage` web -_-)

- [TODO] ALL the tests
- [TODO] Pagination and lazy scroll load
- [TODO] File picker upload
- [TODO] Sync upload (Android & iOS)
- [TODO] Image caching (Android & iOS)
- [TODO] Lots more... TBD


## Running

    # Chrome
    flutter run -d chrome

    # Simulator
    open -a Simulator
    flutter run

## Building

    # Generate GraphQL Flutter Models
    flutter pub run build_runner build
