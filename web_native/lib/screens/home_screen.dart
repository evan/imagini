import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:imagini/blocs/home_bloc.dart';
import 'package:imagini/core/app_provider.dart';

class HomeScreen extends StatefulWidget {
  static const String PATH = '/Home';

  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  HomeBloc bloc;

  int _currentIndex = 0;
  int _totalLength = 1;

  @override
  Widget build(BuildContext context) {
    _init();

    return PlatformScaffold(
      body: _buildBody(),
      bottomNavBar: _buildNavBar()
    );
  }

  void _init(){
    if(bloc != null)
        return;

    bloc = HomeBloc(AppProvider.getApplication(context));
  }

  @override
  void dispose() {
    super.dispose();
    // bloc.dispose();
  }

  Widget _buildBody() {
    var widgetMap = [
      <Widget>[
        _buildAppBar("Gallery"),
        _buildGridView()
      ],
      <Widget>[
        _buildAppBar("Albums"),
        SliverToBoxAdapter()
      ],
      <Widget>[
        _buildAppBar("Settings"),
        SliverToBoxAdapter()
      ],
    ];
    return CustomScrollView(
      shrinkWrap: true,
      slivers: widgetMap[_currentIndex],
    );
  }

  Widget _buildAppBar(String title) {
    return SliverAppBar(
      title: new Text(title),
      pinned: false,
      snap: false,
      floating: true,
      leading: PlatformIconButton(
        icon: Icon(PlatformIcons(context).person),
      ),
      actions: <Widget>[
        PlatformIconButton(
          icon: Icon(PlatformIcons(context).search),
        ),
        PlatformIconButton(
          icon: Icon(PlatformIcons(context).add),
        ),
      ],
    );
  }

  Widget _buildNavBar() {
    return PlatformNavBar(
      currentIndex: _currentIndex,
      itemChanged: (index) => setState(() {
        _currentIndex = index;
      }),
      items: [
        BottomNavigationBarItem(
          label: "Photos",
          icon: Icon(isMaterial(context) ? Icons.insert_photo : CupertinoIcons.photo),
        ),
        BottomNavigationBarItem(
          label: "Albums",
          icon: Icon(isMaterial(context) ? Icons.collections : CupertinoIcons.photo_on_rectangle),
        ),
        BottomNavigationBarItem(
          label: "Settings",
          icon: Icon(PlatformIcons(context).settings),
        ),
      ],
    );
  }

  Widget _appLoading(){
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 500),
        child: Container(
          margin: EdgeInsets.fromLTRB(50, 0, 50, 0),
          height: 370,
          child: Column(
            children: <Widget>[
              PlatformCircularProgressIndicator()
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildGridView() {
    MediaQueryData queryData = MediaQuery.of(context);

    // Can change this to change desired image size
    final int desiredContentWidth = 500;

    return SliverStaggeredGrid.extentBuilder(
      itemCount: _totalLength,
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
      maxCrossAxisExtent: 500 / queryData.devicePixelRatio,
      staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
      itemBuilder: (BuildContext context, int index) {
        return _buildCard(index, desiredContentWidth);
      },
    );
  }

  Widget _buildCard(index, derivedContentWidth) {
    return FutureBuilder<CachedNetworkImage>(
      future: bloc.getMedia(index, derivedContentWidth),
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return SizedBox(width: 500, height: 500);

        WidgetsBinding.instance.addPostFrameCallback((_) {
            if (_totalLength == bloc.totalMediaItems)
              return;

            setState(() {
              _totalLength = bloc.totalMediaItems;
            });
        });

        return snapshot.data;
      }
    );
  }
}
