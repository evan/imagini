// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:meta/meta.dart';
import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:http/http.dart';
import 'package:imagini/helpers/upload_serializer.dart';
part 'imagini_graphql.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class Me$Query$User with EquatableMixin {
  Me$Query$User();

  factory Me$Query$User.fromJson(Map<String, dynamic> json) =>
      _$Me$Query$UserFromJson(json);

  String id;

  DateTime createdAt;

  DateTime updatedAt;

  String email;

  String username;

  String firstName;

  String lastName;

  @JsonKey(unknownEnumValue: Role.artemisUnknown)
  Role role;

  @JsonKey(unknownEnumValue: AuthType.artemisUnknown)
  AuthType authType;

  @override
  List<Object> get props => [
        id,
        createdAt,
        updatedAt,
        email,
        username,
        firstName,
        lastName,
        role,
        authType
      ];
  Map<String, dynamic> toJson() => _$Me$Query$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Me$Query with EquatableMixin {
  Me$Query();

  factory Me$Query.fromJson(Map<String, dynamic> json) =>
      _$Me$QueryFromJson(json);

  Me$Query$User me;

  @override
  List<Object> get props => [me];
  Map<String, dynamic> toJson() => _$Me$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Login$Query$AuthResponse$Device with EquatableMixin {
  Login$Query$AuthResponse$Device();

  factory Login$Query$AuthResponse$Device.fromJson(Map<String, dynamic> json) =>
      _$Login$Query$AuthResponse$DeviceFromJson(json);

  String id;

  @override
  List<Object> get props => [id];
  Map<String, dynamic> toJson() =>
      _$Login$Query$AuthResponse$DeviceToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Login$Query$AuthResponse with EquatableMixin {
  Login$Query$AuthResponse();

  factory Login$Query$AuthResponse.fromJson(Map<String, dynamic> json) =>
      _$Login$Query$AuthResponseFromJson(json);

  @JsonKey(unknownEnumValue: AuthResult.artemisUnknown)
  AuthResult result;

  Login$Query$AuthResponse$Device device;

  @override
  List<Object> get props => [result, device];
  Map<String, dynamic> toJson() => _$Login$Query$AuthResponseToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Login$Query with EquatableMixin {
  Login$Query();

  factory Login$Query.fromJson(Map<String, dynamic> json) =>
      _$Login$QueryFromJson(json);

  Login$Query$AuthResponse login;

  @override
  List<Object> get props => [login];
  Map<String, dynamic> toJson() => _$Login$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateMediaItem$Mutation$MediaItem with EquatableMixin {
  CreateMediaItem$Mutation$MediaItem();

  factory CreateMediaItem$Mutation$MediaItem.fromJson(
          Map<String, dynamic> json) =>
      _$CreateMediaItem$Mutation$MediaItemFromJson(json);

  String id;

  DateTime createdAt;

  DateTime updatedAt;

  DateTime exifDate;

  double latitude;

  double longitude;

  bool isVideo;

  String fileName;

  String origName;

  String userID;

  @override
  List<Object> get props => [
        id,
        createdAt,
        updatedAt,
        exifDate,
        latitude,
        longitude,
        isVideo,
        fileName,
        origName,
        userID
      ];
  Map<String, dynamic> toJson() =>
      _$CreateMediaItem$Mutation$MediaItemToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateMediaItem$Mutation with EquatableMixin {
  CreateMediaItem$Mutation();

  factory CreateMediaItem$Mutation.fromJson(Map<String, dynamic> json) =>
      _$CreateMediaItem$MutationFromJson(json);

  CreateMediaItem$Mutation$MediaItem createMediaItem;

  @override
  List<Object> get props => [createMediaItem];
  Map<String, dynamic> toJson() => _$CreateMediaItem$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MediaItems$Query$MediaItemResponse$MediaItem with EquatableMixin {
  MediaItems$Query$MediaItemResponse$MediaItem();

  factory MediaItems$Query$MediaItemResponse$MediaItem.fromJson(
          Map<String, dynamic> json) =>
      _$MediaItems$Query$MediaItemResponse$MediaItemFromJson(json);

  String id;

  String fileName;

  double latitude;

  double longitude;

  bool isVideo;

  int width;

  int height;

  String origName;

  DateTime createdAt;

  @override
  List<Object> get props => [
        id,
        fileName,
        latitude,
        longitude,
        isVideo,
        width,
        height,
        origName,
        createdAt
      ];
  Map<String, dynamic> toJson() =>
      _$MediaItems$Query$MediaItemResponse$MediaItemToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MediaItems$Query$MediaItemResponse$PageResponse with EquatableMixin {
  MediaItems$Query$MediaItemResponse$PageResponse();

  factory MediaItems$Query$MediaItemResponse$PageResponse.fromJson(
          Map<String, dynamic> json) =>
      _$MediaItems$Query$MediaItemResponse$PageResponseFromJson(json);

  int size;

  int page;

  int total;

  @override
  List<Object> get props => [size, page, total];
  Map<String, dynamic> toJson() =>
      _$MediaItems$Query$MediaItemResponse$PageResponseToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MediaItems$Query$MediaItemResponse with EquatableMixin {
  MediaItems$Query$MediaItemResponse();

  factory MediaItems$Query$MediaItemResponse.fromJson(
          Map<String, dynamic> json) =>
      _$MediaItems$Query$MediaItemResponseFromJson(json);

  List<MediaItems$Query$MediaItemResponse$MediaItem> data;

  MediaItems$Query$MediaItemResponse$PageResponse page;

  @override
  List<Object> get props => [data, page];
  Map<String, dynamic> toJson() =>
      _$MediaItems$Query$MediaItemResponseToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MediaItems$Query with EquatableMixin {
  MediaItems$Query();

  factory MediaItems$Query.fromJson(Map<String, dynamic> json) =>
      _$MediaItems$QueryFromJson(json);

  MediaItems$Query$MediaItemResponse mediaItems;

  @override
  List<Object> get props => [mediaItems];
  Map<String, dynamic> toJson() => _$MediaItems$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class TimeFilter with EquatableMixin {
  TimeFilter(
      {this.equalTo,
      this.notEqualTo,
      this.lessThan,
      this.lessThanOrEqualTo,
      this.greaterThan,
      this.greaterThanOrEqualTo});

  factory TimeFilter.fromJson(Map<String, dynamic> json) =>
      _$TimeFilterFromJson(json);

  DateTime equalTo;

  DateTime notEqualTo;

  DateTime lessThan;

  DateTime lessThanOrEqualTo;

  DateTime greaterThan;

  DateTime greaterThanOrEqualTo;

  @override
  List<Object> get props => [
        equalTo,
        notEqualTo,
        lessThan,
        lessThanOrEqualTo,
        greaterThan,
        greaterThanOrEqualTo
      ];
  Map<String, dynamic> toJson() => _$TimeFilterToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FloatFilter with EquatableMixin {
  FloatFilter(
      {this.equalTo,
      this.notEqualTo,
      this.lessThan,
      this.lessThanOrEqualTo,
      this.greaterThan,
      this.greaterThanOrEqualTo});

  factory FloatFilter.fromJson(Map<String, dynamic> json) =>
      _$FloatFilterFromJson(json);

  double equalTo;

  double notEqualTo;

  double lessThan;

  double lessThanOrEqualTo;

  double greaterThan;

  double greaterThanOrEqualTo;

  @override
  List<Object> get props => [
        equalTo,
        notEqualTo,
        lessThan,
        lessThanOrEqualTo,
        greaterThan,
        greaterThanOrEqualTo
      ];
  Map<String, dynamic> toJson() => _$FloatFilterToJson(this);
}

@JsonSerializable(explicitToJson: true)
class BooleanFilter with EquatableMixin {
  BooleanFilter({this.equalTo, this.notEqualTo});

  factory BooleanFilter.fromJson(Map<String, dynamic> json) =>
      _$BooleanFilterFromJson(json);

  bool equalTo;

  bool notEqualTo;

  @override
  List<Object> get props => [equalTo, notEqualTo];
  Map<String, dynamic> toJson() => _$BooleanFilterToJson(this);
}

@JsonSerializable(explicitToJson: true)
class IDFilter with EquatableMixin {
  IDFilter({this.equalTo, this.notEqualTo});

  factory IDFilter.fromJson(Map<String, dynamic> json) =>
      _$IDFilterFromJson(json);

  String equalTo;

  String notEqualTo;

  @override
  List<Object> get props => [equalTo, notEqualTo];
  Map<String, dynamic> toJson() => _$IDFilterToJson(this);
}

@JsonSerializable(explicitToJson: true)
class StringFilter with EquatableMixin {
  StringFilter(
      {this.equalTo,
      this.notEqualTo,
      this.startsWith,
      this.notStartsWith,
      this.endsWith,
      this.notEndsWith,
      this.contains,
      this.notContains});

  factory StringFilter.fromJson(Map<String, dynamic> json) =>
      _$StringFilterFromJson(json);

  String equalTo;

  String notEqualTo;

  String startsWith;

  String notStartsWith;

  String endsWith;

  String notEndsWith;

  String contains;

  String notContains;

  @override
  List<Object> get props => [
        equalTo,
        notEqualTo,
        startsWith,
        notStartsWith,
        endsWith,
        notEndsWith,
        contains,
        notContains
      ];
  Map<String, dynamic> toJson() => _$StringFilterToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MediaItemFilter with EquatableMixin {
  MediaItemFilter(
      {this.id,
      this.createdAt,
      this.updatedAt,
      this.exifDate,
      this.latitude,
      this.longitude,
      this.isVideo,
      this.origName,
      this.tags,
      this.albums});

  factory MediaItemFilter.fromJson(Map<String, dynamic> json) =>
      _$MediaItemFilterFromJson(json);

  IDFilter id;

  TimeFilter createdAt;

  TimeFilter updatedAt;

  TimeFilter exifDate;

  FloatFilter latitude;

  FloatFilter longitude;

  BooleanFilter isVideo;

  StringFilter origName;

  TagFilter tags;

  AlbumFilter albums;

  @override
  List<Object> get props => [
        id,
        createdAt,
        updatedAt,
        exifDate,
        latitude,
        longitude,
        isVideo,
        origName,
        tags,
        albums
      ];
  Map<String, dynamic> toJson() => _$MediaItemFilterToJson(this);
}

@JsonSerializable(explicitToJson: true)
class TagFilter with EquatableMixin {
  TagFilter({this.id, this.createdAt, this.updatedAt, this.name});

  factory TagFilter.fromJson(Map<String, dynamic> json) =>
      _$TagFilterFromJson(json);

  IDFilter id;

  TimeFilter createdAt;

  TimeFilter updatedAt;

  StringFilter name;

  @override
  List<Object> get props => [id, createdAt, updatedAt, name];
  Map<String, dynamic> toJson() => _$TagFilterToJson(this);
}

@JsonSerializable(explicitToJson: true)
class AlbumFilter with EquatableMixin {
  AlbumFilter({this.id, this.createdAt, this.updatedAt, this.name});

  factory AlbumFilter.fromJson(Map<String, dynamic> json) =>
      _$AlbumFilterFromJson(json);

  IDFilter id;

  TimeFilter createdAt;

  TimeFilter updatedAt;

  StringFilter name;

  @override
  List<Object> get props => [id, createdAt, updatedAt, name];
  Map<String, dynamic> toJson() => _$AlbumFilterToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Page with EquatableMixin {
  Page({this.size, this.page});

  factory Page.fromJson(Map<String, dynamic> json) => _$PageFromJson(json);

  int size;

  int page;

  @override
  List<Object> get props => [size, page];
  Map<String, dynamic> toJson() => _$PageToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Order with EquatableMixin {
  Order({this.by, this.direction});

  factory Order.fromJson(Map<String, dynamic> json) => _$OrderFromJson(json);

  String by;

  @JsonKey(unknownEnumValue: OrderDirection.artemisUnknown)
  OrderDirection direction;

  @override
  List<Object> get props => [by, direction];
  Map<String, dynamic> toJson() => _$OrderToJson(this);
}

enum Role {
  @JsonValue('Admin')
  admin,
  @JsonValue('User')
  user,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}
enum AuthType {
  @JsonValue('Local')
  local,
  @JsonValue('LDAP')
  ldap,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}
enum AuthResult {
  @JsonValue('Success')
  success,
  @JsonValue('Failure')
  failure,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}
enum OrderDirection {
  @JsonValue('ASC')
  asc,
  @JsonValue('DESC')
  desc,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

class MeQuery extends GraphQLQuery<Me$Query, JsonSerializable> {
  MeQuery();

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'me'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'me'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'createdAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'updatedAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'email'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'username'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'firstName'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'lastName'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'role'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'authType'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'me';

  @override
  List<Object> get props => [document, operationName];
  @override
  Me$Query parse(Map<String, dynamic> json) => Me$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class LoginArguments extends JsonSerializable with EquatableMixin {
  LoginArguments({@required this.user, @required this.password, this.deviceID});

  @override
  factory LoginArguments.fromJson(Map<String, dynamic> json) =>
      _$LoginArgumentsFromJson(json);

  final String user;

  final String password;

  final String deviceID;

  @override
  List<Object> get props => [user, password, deviceID];
  @override
  Map<String, dynamic> toJson() => _$LoginArgumentsToJson(this);
}

class LoginQuery extends GraphQLQuery<Login$Query, LoginArguments> {
  LoginQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'login'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'user')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'password')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'deviceID')),
              type:
                  NamedTypeNode(name: NameNode(value: 'ID'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'login'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'user'),
                    value: VariableNode(name: NameNode(value: 'user'))),
                ArgumentNode(
                    name: NameNode(value: 'password'),
                    value: VariableNode(name: NameNode(value: 'password'))),
                ArgumentNode(
                    name: NameNode(value: 'deviceID'),
                    value: VariableNode(name: NameNode(value: 'deviceID')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'result'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'device'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'login';

  @override
  final LoginArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  Login$Query parse(Map<String, dynamic> json) => Login$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class CreateMediaItemArguments extends JsonSerializable with EquatableMixin {
  CreateMediaItemArguments({@required this.file});

  @override
  factory CreateMediaItemArguments.fromJson(Map<String, dynamic> json) =>
      _$CreateMediaItemArgumentsFromJson(json);

  @JsonKey(
      fromJson: fromGraphQLUploadToDartMultipartFile,
      toJson: fromDartMultipartFileToGraphQLUpload)
  final MultipartFile file;

  @override
  List<Object> get props => [file];
  @override
  Map<String, dynamic> toJson() => _$CreateMediaItemArgumentsToJson(this);
}

class CreateMediaItemMutation
    extends GraphQLQuery<CreateMediaItem$Mutation, CreateMediaItemArguments> {
  CreateMediaItemMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'createMediaItem'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'file')),
              type: NamedTypeNode(
                  name: NameNode(value: 'Upload'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'createMediaItem'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: ObjectValueNode(fields: [
                      ObjectFieldNode(
                          name: NameNode(value: 'file'),
                          value: VariableNode(name: NameNode(value: 'file')))
                    ]))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'id'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'createdAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'updatedAt'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'exifDate'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'latitude'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'longitude'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'isVideo'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'fileName'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'origName'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'userID'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'createMediaItem';

  @override
  final CreateMediaItemArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  CreateMediaItem$Mutation parse(Map<String, dynamic> json) =>
      CreateMediaItem$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class MediaItemsArguments extends JsonSerializable with EquatableMixin {
  MediaItemsArguments({this.order, this.page, this.filter});

  @override
  factory MediaItemsArguments.fromJson(Map<String, dynamic> json) =>
      _$MediaItemsArgumentsFromJson(json);

  final Order order;

  final Page page;

  final MediaItemFilter filter;

  @override
  List<Object> get props => [order, page, filter];
  @override
  Map<String, dynamic> toJson() => _$MediaItemsArgumentsToJson(this);
}

class MediaItemsQuery
    extends GraphQLQuery<MediaItems$Query, MediaItemsArguments> {
  MediaItemsQuery({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'mediaItems'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'order')),
              type: NamedTypeNode(
                  name: NameNode(value: 'Order'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'page')),
              type: NamedTypeNode(
                  name: NameNode(value: 'Page'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'filter')),
              type: NamedTypeNode(
                  name: NameNode(value: 'MediaItemFilter'), isNonNull: false),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'mediaItems'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'filter'),
                    value: VariableNode(name: NameNode(value: 'filter'))),
                ArgumentNode(
                    name: NameNode(value: 'page'),
                    value: VariableNode(name: NameNode(value: 'page'))),
                ArgumentNode(
                    name: NameNode(value: 'order'),
                    value: VariableNode(name: NameNode(value: 'order')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'data'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'fileName'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'latitude'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'longitude'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'isVideo'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'width'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'height'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'origName'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'createdAt'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ])),
                FieldNode(
                    name: NameNode(value: 'page'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'size'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'page'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'total'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'mediaItems';

  @override
  final MediaItemsArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  MediaItems$Query parse(Map<String, dynamic> json) =>
      MediaItems$Query.fromJson(json);
}
