// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'imagini_graphql.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Me$Query$User _$Me$Query$UserFromJson(Map<String, dynamic> json) {
  return Me$Query$User()
    ..id = json['id'] as String
    ..createdAt = json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String)
    ..email = json['email'] as String
    ..username = json['username'] as String
    ..firstName = json['firstName'] as String
    ..lastName = json['lastName'] as String
    ..role = _$enumDecodeNullable(_$RoleEnumMap, json['role'],
        unknownValue: Role.artemisUnknown)
    ..authType = _$enumDecodeNullable(_$AuthTypeEnumMap, json['authType'],
        unknownValue: AuthType.artemisUnknown);
}

Map<String, dynamic> _$Me$Query$UserToJson(Me$Query$User instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'email': instance.email,
      'username': instance.username,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'role': _$RoleEnumMap[instance.role],
      'authType': _$AuthTypeEnumMap[instance.authType],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$RoleEnumMap = {
  Role.admin: 'Admin',
  Role.user: 'User',
  Role.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$AuthTypeEnumMap = {
  AuthType.local: 'Local',
  AuthType.ldap: 'LDAP',
  AuthType.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

Me$Query _$Me$QueryFromJson(Map<String, dynamic> json) {
  return Me$Query()
    ..me = json['me'] == null
        ? null
        : Me$Query$User.fromJson(json['me'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Me$QueryToJson(Me$Query instance) => <String, dynamic>{
      'me': instance.me?.toJson(),
    };

Login$Query$AuthResponse$Device _$Login$Query$AuthResponse$DeviceFromJson(
    Map<String, dynamic> json) {
  return Login$Query$AuthResponse$Device()..id = json['id'] as String;
}

Map<String, dynamic> _$Login$Query$AuthResponse$DeviceToJson(
        Login$Query$AuthResponse$Device instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

Login$Query$AuthResponse _$Login$Query$AuthResponseFromJson(
    Map<String, dynamic> json) {
  return Login$Query$AuthResponse()
    ..result = _$enumDecodeNullable(_$AuthResultEnumMap, json['result'],
        unknownValue: AuthResult.artemisUnknown)
    ..device = json['device'] == null
        ? null
        : Login$Query$AuthResponse$Device.fromJson(
            json['device'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Login$Query$AuthResponseToJson(
        Login$Query$AuthResponse instance) =>
    <String, dynamic>{
      'result': _$AuthResultEnumMap[instance.result],
      'device': instance.device?.toJson(),
    };

const _$AuthResultEnumMap = {
  AuthResult.success: 'Success',
  AuthResult.failure: 'Failure',
  AuthResult.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

Login$Query _$Login$QueryFromJson(Map<String, dynamic> json) {
  return Login$Query()
    ..login = json['login'] == null
        ? null
        : Login$Query$AuthResponse.fromJson(
            json['login'] as Map<String, dynamic>);
}

Map<String, dynamic> _$Login$QueryToJson(Login$Query instance) =>
    <String, dynamic>{
      'login': instance.login?.toJson(),
    };

CreateMediaItem$Mutation$MediaItem _$CreateMediaItem$Mutation$MediaItemFromJson(
    Map<String, dynamic> json) {
  return CreateMediaItem$Mutation$MediaItem()
    ..id = json['id'] as String
    ..createdAt = json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String)
    ..exifDate = json['exifDate'] == null
        ? null
        : DateTime.parse(json['exifDate'] as String)
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..isVideo = json['isVideo'] as bool
    ..fileName = json['fileName'] as String
    ..origName = json['origName'] as String
    ..userID = json['userID'] as String;
}

Map<String, dynamic> _$CreateMediaItem$Mutation$MediaItemToJson(
        CreateMediaItem$Mutation$MediaItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'exifDate': instance.exifDate?.toIso8601String(),
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'isVideo': instance.isVideo,
      'fileName': instance.fileName,
      'origName': instance.origName,
      'userID': instance.userID,
    };

CreateMediaItem$Mutation _$CreateMediaItem$MutationFromJson(
    Map<String, dynamic> json) {
  return CreateMediaItem$Mutation()
    ..createMediaItem = json['createMediaItem'] == null
        ? null
        : CreateMediaItem$Mutation$MediaItem.fromJson(
            json['createMediaItem'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateMediaItem$MutationToJson(
        CreateMediaItem$Mutation instance) =>
    <String, dynamic>{
      'createMediaItem': instance.createMediaItem?.toJson(),
    };

MediaItems$Query$MediaItemResponse$MediaItem
    _$MediaItems$Query$MediaItemResponse$MediaItemFromJson(
        Map<String, dynamic> json) {
  return MediaItems$Query$MediaItemResponse$MediaItem()
    ..id = json['id'] as String
    ..fileName = json['fileName'] as String
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..isVideo = json['isVideo'] as bool
    ..width = json['width'] as int
    ..height = json['height'] as int
    ..origName = json['origName'] as String
    ..createdAt = json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String);
}

Map<String, dynamic> _$MediaItems$Query$MediaItemResponse$MediaItemToJson(
        MediaItems$Query$MediaItemResponse$MediaItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'fileName': instance.fileName,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'isVideo': instance.isVideo,
      'width': instance.width,
      'height': instance.height,
      'origName': instance.origName,
      'createdAt': instance.createdAt?.toIso8601String(),
    };

MediaItems$Query$MediaItemResponse$PageResponse
    _$MediaItems$Query$MediaItemResponse$PageResponseFromJson(
        Map<String, dynamic> json) {
  return MediaItems$Query$MediaItemResponse$PageResponse()
    ..size = json['size'] as int
    ..page = json['page'] as int
    ..total = json['total'] as int;
}

Map<String, dynamic> _$MediaItems$Query$MediaItemResponse$PageResponseToJson(
        MediaItems$Query$MediaItemResponse$PageResponse instance) =>
    <String, dynamic>{
      'size': instance.size,
      'page': instance.page,
      'total': instance.total,
    };

MediaItems$Query$MediaItemResponse _$MediaItems$Query$MediaItemResponseFromJson(
    Map<String, dynamic> json) {
  return MediaItems$Query$MediaItemResponse()
    ..data = (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : MediaItems$Query$MediaItemResponse$MediaItem.fromJson(
                e as Map<String, dynamic>))
        ?.toList()
    ..page = json['page'] == null
        ? null
        : MediaItems$Query$MediaItemResponse$PageResponse.fromJson(
            json['page'] as Map<String, dynamic>);
}

Map<String, dynamic> _$MediaItems$Query$MediaItemResponseToJson(
        MediaItems$Query$MediaItemResponse instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
      'page': instance.page?.toJson(),
    };

MediaItems$Query _$MediaItems$QueryFromJson(Map<String, dynamic> json) {
  return MediaItems$Query()
    ..mediaItems = json['mediaItems'] == null
        ? null
        : MediaItems$Query$MediaItemResponse.fromJson(
            json['mediaItems'] as Map<String, dynamic>);
}

Map<String, dynamic> _$MediaItems$QueryToJson(MediaItems$Query instance) =>
    <String, dynamic>{
      'mediaItems': instance.mediaItems?.toJson(),
    };

TimeFilter _$TimeFilterFromJson(Map<String, dynamic> json) {
  return TimeFilter(
    equalTo: json['equalTo'] == null
        ? null
        : DateTime.parse(json['equalTo'] as String),
    notEqualTo: json['notEqualTo'] == null
        ? null
        : DateTime.parse(json['notEqualTo'] as String),
    lessThan: json['lessThan'] == null
        ? null
        : DateTime.parse(json['lessThan'] as String),
    lessThanOrEqualTo: json['lessThanOrEqualTo'] == null
        ? null
        : DateTime.parse(json['lessThanOrEqualTo'] as String),
    greaterThan: json['greaterThan'] == null
        ? null
        : DateTime.parse(json['greaterThan'] as String),
    greaterThanOrEqualTo: json['greaterThanOrEqualTo'] == null
        ? null
        : DateTime.parse(json['greaterThanOrEqualTo'] as String),
  );
}

Map<String, dynamic> _$TimeFilterToJson(TimeFilter instance) =>
    <String, dynamic>{
      'equalTo': instance.equalTo?.toIso8601String(),
      'notEqualTo': instance.notEqualTo?.toIso8601String(),
      'lessThan': instance.lessThan?.toIso8601String(),
      'lessThanOrEqualTo': instance.lessThanOrEqualTo?.toIso8601String(),
      'greaterThan': instance.greaterThan?.toIso8601String(),
      'greaterThanOrEqualTo': instance.greaterThanOrEqualTo?.toIso8601String(),
    };

FloatFilter _$FloatFilterFromJson(Map<String, dynamic> json) {
  return FloatFilter(
    equalTo: (json['equalTo'] as num)?.toDouble(),
    notEqualTo: (json['notEqualTo'] as num)?.toDouble(),
    lessThan: (json['lessThan'] as num)?.toDouble(),
    lessThanOrEqualTo: (json['lessThanOrEqualTo'] as num)?.toDouble(),
    greaterThan: (json['greaterThan'] as num)?.toDouble(),
    greaterThanOrEqualTo: (json['greaterThanOrEqualTo'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$FloatFilterToJson(FloatFilter instance) =>
    <String, dynamic>{
      'equalTo': instance.equalTo,
      'notEqualTo': instance.notEqualTo,
      'lessThan': instance.lessThan,
      'lessThanOrEqualTo': instance.lessThanOrEqualTo,
      'greaterThan': instance.greaterThan,
      'greaterThanOrEqualTo': instance.greaterThanOrEqualTo,
    };

BooleanFilter _$BooleanFilterFromJson(Map<String, dynamic> json) {
  return BooleanFilter(
    equalTo: json['equalTo'] as bool,
    notEqualTo: json['notEqualTo'] as bool,
  );
}

Map<String, dynamic> _$BooleanFilterToJson(BooleanFilter instance) =>
    <String, dynamic>{
      'equalTo': instance.equalTo,
      'notEqualTo': instance.notEqualTo,
    };

IDFilter _$IDFilterFromJson(Map<String, dynamic> json) {
  return IDFilter(
    equalTo: json['equalTo'] as String,
    notEqualTo: json['notEqualTo'] as String,
  );
}

Map<String, dynamic> _$IDFilterToJson(IDFilter instance) => <String, dynamic>{
      'equalTo': instance.equalTo,
      'notEqualTo': instance.notEqualTo,
    };

StringFilter _$StringFilterFromJson(Map<String, dynamic> json) {
  return StringFilter(
    equalTo: json['equalTo'] as String,
    notEqualTo: json['notEqualTo'] as String,
    startsWith: json['startsWith'] as String,
    notStartsWith: json['notStartsWith'] as String,
    endsWith: json['endsWith'] as String,
    notEndsWith: json['notEndsWith'] as String,
    contains: json['contains'] as String,
    notContains: json['notContains'] as String,
  );
}

Map<String, dynamic> _$StringFilterToJson(StringFilter instance) =>
    <String, dynamic>{
      'equalTo': instance.equalTo,
      'notEqualTo': instance.notEqualTo,
      'startsWith': instance.startsWith,
      'notStartsWith': instance.notStartsWith,
      'endsWith': instance.endsWith,
      'notEndsWith': instance.notEndsWith,
      'contains': instance.contains,
      'notContains': instance.notContains,
    };

MediaItemFilter _$MediaItemFilterFromJson(Map<String, dynamic> json) {
  return MediaItemFilter(
    id: json['id'] == null
        ? null
        : IDFilter.fromJson(json['id'] as Map<String, dynamic>),
    createdAt: json['createdAt'] == null
        ? null
        : TimeFilter.fromJson(json['createdAt'] as Map<String, dynamic>),
    updatedAt: json['updatedAt'] == null
        ? null
        : TimeFilter.fromJson(json['updatedAt'] as Map<String, dynamic>),
    exifDate: json['exifDate'] == null
        ? null
        : TimeFilter.fromJson(json['exifDate'] as Map<String, dynamic>),
    latitude: json['latitude'] == null
        ? null
        : FloatFilter.fromJson(json['latitude'] as Map<String, dynamic>),
    longitude: json['longitude'] == null
        ? null
        : FloatFilter.fromJson(json['longitude'] as Map<String, dynamic>),
    isVideo: json['isVideo'] == null
        ? null
        : BooleanFilter.fromJson(json['isVideo'] as Map<String, dynamic>),
    origName: json['origName'] == null
        ? null
        : StringFilter.fromJson(json['origName'] as Map<String, dynamic>),
    tags: json['tags'] == null
        ? null
        : TagFilter.fromJson(json['tags'] as Map<String, dynamic>),
    albums: json['albums'] == null
        ? null
        : AlbumFilter.fromJson(json['albums'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$MediaItemFilterToJson(MediaItemFilter instance) =>
    <String, dynamic>{
      'id': instance.id?.toJson(),
      'createdAt': instance.createdAt?.toJson(),
      'updatedAt': instance.updatedAt?.toJson(),
      'exifDate': instance.exifDate?.toJson(),
      'latitude': instance.latitude?.toJson(),
      'longitude': instance.longitude?.toJson(),
      'isVideo': instance.isVideo?.toJson(),
      'origName': instance.origName?.toJson(),
      'tags': instance.tags?.toJson(),
      'albums': instance.albums?.toJson(),
    };

TagFilter _$TagFilterFromJson(Map<String, dynamic> json) {
  return TagFilter(
    id: json['id'] == null
        ? null
        : IDFilter.fromJson(json['id'] as Map<String, dynamic>),
    createdAt: json['createdAt'] == null
        ? null
        : TimeFilter.fromJson(json['createdAt'] as Map<String, dynamic>),
    updatedAt: json['updatedAt'] == null
        ? null
        : TimeFilter.fromJson(json['updatedAt'] as Map<String, dynamic>),
    name: json['name'] == null
        ? null
        : StringFilter.fromJson(json['name'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TagFilterToJson(TagFilter instance) => <String, dynamic>{
      'id': instance.id?.toJson(),
      'createdAt': instance.createdAt?.toJson(),
      'updatedAt': instance.updatedAt?.toJson(),
      'name': instance.name?.toJson(),
    };

AlbumFilter _$AlbumFilterFromJson(Map<String, dynamic> json) {
  return AlbumFilter(
    id: json['id'] == null
        ? null
        : IDFilter.fromJson(json['id'] as Map<String, dynamic>),
    createdAt: json['createdAt'] == null
        ? null
        : TimeFilter.fromJson(json['createdAt'] as Map<String, dynamic>),
    updatedAt: json['updatedAt'] == null
        ? null
        : TimeFilter.fromJson(json['updatedAt'] as Map<String, dynamic>),
    name: json['name'] == null
        ? null
        : StringFilter.fromJson(json['name'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AlbumFilterToJson(AlbumFilter instance) =>
    <String, dynamic>{
      'id': instance.id?.toJson(),
      'createdAt': instance.createdAt?.toJson(),
      'updatedAt': instance.updatedAt?.toJson(),
      'name': instance.name?.toJson(),
    };

Page _$PageFromJson(Map<String, dynamic> json) {
  return Page(
    size: json['size'] as int,
    page: json['page'] as int,
  );
}

Map<String, dynamic> _$PageToJson(Page instance) => <String, dynamic>{
      'size': instance.size,
      'page': instance.page,
    };

Order _$OrderFromJson(Map<String, dynamic> json) {
  return Order(
    by: json['by'] as String,
    direction: _$enumDecodeNullable(_$OrderDirectionEnumMap, json['direction'],
        unknownValue: OrderDirection.artemisUnknown),
  );
}

Map<String, dynamic> _$OrderToJson(Order instance) => <String, dynamic>{
      'by': instance.by,
      'direction': _$OrderDirectionEnumMap[instance.direction],
    };

const _$OrderDirectionEnumMap = {
  OrderDirection.asc: 'ASC',
  OrderDirection.desc: 'DESC',
  OrderDirection.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

LoginArguments _$LoginArgumentsFromJson(Map<String, dynamic> json) {
  return LoginArguments(
    user: json['user'] as String,
    password: json['password'] as String,
    deviceID: json['deviceID'] as String,
  );
}

Map<String, dynamic> _$LoginArgumentsToJson(LoginArguments instance) =>
    <String, dynamic>{
      'user': instance.user,
      'password': instance.password,
      'deviceID': instance.deviceID,
    };

CreateMediaItemArguments _$CreateMediaItemArgumentsFromJson(
    Map<String, dynamic> json) {
  return CreateMediaItemArguments(
    file: fromGraphQLUploadToDartMultipartFile(json['file'] as MultipartFile),
  );
}

Map<String, dynamic> _$CreateMediaItemArgumentsToJson(
        CreateMediaItemArguments instance) =>
    <String, dynamic>{
      'file': fromDartMultipartFileToGraphQLUpload(instance.file),
    };

MediaItemsArguments _$MediaItemsArgumentsFromJson(Map<String, dynamic> json) {
  return MediaItemsArguments(
    order: json['order'] == null
        ? null
        : Order.fromJson(json['order'] as Map<String, dynamic>),
    page: json['page'] == null
        ? null
        : Page.fromJson(json['page'] as Map<String, dynamic>),
    filter: json['filter'] == null
        ? null
        : MediaItemFilter.fromJson(json['filter'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$MediaItemsArgumentsToJson(
        MediaItemsArguments instance) =>
    <String, dynamic>{
      'order': instance.order?.toJson(),
      'page': instance.page?.toJson(),
      'filter': instance.filter?.toJson(),
    };
