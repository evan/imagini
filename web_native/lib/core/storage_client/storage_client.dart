import './base_storage_client.dart';

BaseStorageClient getStorageClient() => throw UnsupportedError(
    'Cannot create a storage client.');
