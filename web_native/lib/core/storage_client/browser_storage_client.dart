import 'dart:html';

import './base_storage_client.dart';

BaseStorageClient getStorageClient() => BrowserStorageClient();

class BrowserStorageClient extends BaseStorageClient {
  @override
  Future<String> get(String key) async {
      var requestedValue = window.localStorage.containsKey(key) ? window.localStorage[key] : "";
      return requestedValue;
  }

  @override
  Future<void> set(String key, String value) async {
      window.localStorage[key] = value;
  }
}
