import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import './base_storage_client.dart';

BaseStorageClient getStorageClient() => MobileStorageClient();

class MobileStorageClient extends BaseStorageClient {
  final storage = new FlutterSecureStorage();

  @override
  Future<String> get(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> set(String key, String value) async {
    return storage.write(key: key, value: value);
  }
}
