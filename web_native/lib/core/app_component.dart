import 'package:flutter/material.dart';

import 'package:imagini/core/env.dart';
import 'package:imagini/core/app_provider.dart';
import 'package:imagini/core/imagini_application.dart';

class AppComponent extends StatefulWidget {

  final ImaginiApplication _application;

  AppComponent(this._application);

  @override
  State createState() {
    return new AppComponentState(_application);
  }
}

class AppComponentState extends State<AppComponent> {

  final ImaginiApplication _application;

  AppComponentState(this._application);

  @override
  void dispose() async {
    super.dispose();
    await _application.onTerminate();
  }

  @override
  Widget build(BuildContext context) {

    final app = new MaterialApp(
        title: Env.value.appName,
        debugShowCheckedModeBanner: false,
        theme: new ThemeData(
          primarySwatch: Colors.blue,
        ),
        onGenerateRoute: _application.router.generator,
    );
    print('initial core.route = ${app.initialRoute}');

    final appProvider = AppProvider(child: app, application: _application);
    return appProvider;
  }
}
