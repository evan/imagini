import 'package:flutter/material.dart';

import 'package:imagini/core/app_component.dart';
import 'package:imagini/core/imagini_application.dart';

class Env {
    static Env value;
    String appName;

    Env() {
        value = this;
        _init();
    }

    _init() async {
        WidgetsFlutterBinding.ensureInitialized();
        var application = ImaginiApplication();
        await application.onCreate();
        runApp(AppComponent(application));
    }
}
