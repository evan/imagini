import 'package:fluro/fluro.dart';

import 'package:imagini/core/app_routes.dart';
import 'package:imagini/api/api_provider.dart';
import 'package:imagini/api/imagini_api_repository.dart';
import 'package:imagini/core/storage_client/base_storage_client.dart';

import 'package:imagini/core/storage_client/storage_client.dart'
    if (dart.library.html) 'package:imagini/core/storage_client/browser_storage_client.dart'
    if (dart.library.io) 'package:imagini/core/storage_client/mobile_storage_client.dart';

class ImaginiApplication {
    FluroRouter router;
    ImaginiAPIRepository imaginiAPI;
    BaseStorageClient storageClient;

    Future<void> onCreate() async {
        _initRouter();
        _initStorageClient();
        await _initAPIRepository();
    }

    Future<void> onTerminate() async {}

    _initRouter() {
        router = new FluroRouter();
        AppRoutes.configureRoutes(router);
    }

    _initStorageClient() {
        storageClient = getStorageClient();
    }

    _initAPIRepository() async {
        // TODO: Get from config
        APIProvider apiProvider = new APIProvider(storageClient);
        await apiProvider.init();
        imaginiAPI = ImaginiAPIRepository(apiProvider);
    }
}
