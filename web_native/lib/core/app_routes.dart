import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';

import 'package:imagini/screens/home_screen.dart';
import 'package:imagini/screens/login_screen.dart';
// import 'package:imagini/screens/splash_screen.dart';

// var splashHandler = new Handler(
//   handlerFunc: (BuildContext context, Map<String, List<String>> params) {
//       return SplashScreen();
//   }
// );

var loginHandler = new Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return LoginScreen();
  }
);

var homeHandler = new Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return HomeScreen();
  }
);

// var appDetailRouteHandler = new Handler(
//     handlerFunc: (BuildContext context, Map<String, List<String>> params) {
//         String appId = params['appId']?.first;
//         String heroTag = params['heroTag']?.first;
//         String title = params['title']?.first;
//         String url = params['url']?.first;
//         String titleTag = params['titleTag']?.first;
// 
//         return new AppDetailPage(appId: num.parse(appId), heroTag:heroTag,title: title, url: url, titleTag: titleTag);
//     });

class AppRoutes {
  static void configureRoutes(FluroRouter router) {
      router.notFoundHandler = new Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
            print('ROUTE WAS NOT FOUND !!!');
            return;
        }
      );
      // router.define(SplashScreen.PATH, handler: splashHandler);
      router.define(LoginScreen.PATH,  handler: loginHandler);
      router.define(HomeScreen.PATH,   handler: homeHandler);
      // router.define(AppDetailPage.PATH, handler: appDetailRouteHandler);
  }
}
