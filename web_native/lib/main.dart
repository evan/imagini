import 'package:imagini/core/env.dart';

void main() => ProductionImagini();

class ProductionImagini extends Env {
    final String appName = "Imagini";
}
