import 'dart:async';

import 'package:imagini/core/imagini_application.dart';
import 'package:imagini/api/imagini_api_repository.dart';

class LoginBloc{

  final ImaginiApplication _application;
  ImaginiAPIRepository _imaginiAPI;

  final _loginController = StreamController<bool>.broadcast();
  Stream<bool> get loginResult => _loginController.stream;

  final _authenticatedController = StreamController<bool>.broadcast();
  Stream<bool> get authenticatedResult => _authenticatedController.stream;

  LoginBloc(this._application){
    _init();
  }

  void _init(){
    _imaginiAPI = _application.imaginiAPI;
    checkAuthentication();

    // attemptLogin("admin", "admin", "http://localhost:8484");
  }

  void dispose(){
    _loginController.close();
    _authenticatedController.close();
  }

  checkAuthentication(){
    _authenticatedController.addStream(_imaginiAPI.isAuthenticated());
  }

  attemptLogin(String username, password, server){
    _loginController.addStream(_imaginiAPI.login(username, password, server));
  }
}
