import 'dart:async';
import 'package:imagini/api/api_provider.dart';
import 'package:imagini/graphql/imagini_graphql.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ImaginiAPIRepository {
  APIProvider _apiProvider;

  ImaginiAPIRepository(this._apiProvider);

  Stream<bool> login(String user, password, server) {
    return Stream.fromFuture(_apiProvider.login(user, password, server).then((QueryResult resp) {
      if (resp.exception != null)
        return false;

      final loginResponse = Login$Query.fromJson(resp.data);
      if (loginResponse.login.result == AuthResult.failure)
        return false;

      return true;
    }));
  }

  Stream<QueryResult> me() {
    return Stream.fromFuture(_apiProvider.me());
  }

  Future<MediaItems$Query> mediaItems(Page page) async {
    QueryResult allItems = await _apiProvider.mediaItems(page);
    return MediaItems$Query.fromJson(allItems.data);
  }

  Stream<bool> isAuthenticated() {
    return Stream.fromFuture(_apiProvider.me().then((QueryResult resp) {
      if (resp.exception != null)
        return false;
      return true;
    }));
  }

  Future<CachedNetworkImage> getImage(String fileName, int width, int height) {
    return _apiProvider.getImage(fileName, width, height);
  }
}
