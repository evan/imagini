import 'dart:async';
import 'package:flutter/material.dart' show BoxFit, Center, SizedBox;

import 'package:imagini/api/cookie_client/cookie_client.dart'
    if (dart.library.html) 'package:imagini/api/cookie_client/browser_cookie_client.dart'
    if (dart.library.io) 'package:imagini/api/cookie_client/io_cookie_client.dart';

import 'package:imagini/core/storage_client/base_storage_client.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:imagini/graphql/imagini_graphql.dart';

class APIProvider{
  static const String _GRAPHQL_ENDPOINT = "/query";

  BaseStorageClient _storage;
  GraphQLClient _client;
  HttpLink httpLink;

  APIProvider(BaseStorageClient storage) {
    _storage = storage;
  }

  Future<void> init() async {
    String _server = await _storage.get("server");

    // Initialize
    if (_server == null)
        _server = "http://localhost";

    httpLink = HttpLink(_server + _GRAPHQL_ENDPOINT,
      httpClient: getCookieClient(_storage),
    );

    _client = GraphQLClient(
      cache: GraphQLCache(),
      link: httpLink,
    );
  }

  Future<QueryResult> login(
    String username,
    String password,
    String server,
  ) async {
    assert(
      (username != null && password != null && server != null)
    );

    // Initialize New Connection
    await _storage.set("server", server);
    await init();

    QueryResult response = await _client.query(
      QueryOptions(
        document: LoginQuery().document,
        variables: {
          "user": username,
          "password": password
        },
      )
    );
    return response;
  }

  Future<CachedNetworkImage> getImage(String fileName, int width, int height) async {
    String server = await _storage.get("server");
    String accessToken = await _storage.get("accessToken");
    String refreshToken = await _storage.get("refreshToken");

    String fullURL = "$server/media/$fileName?width=$width";
    print(fullURL);
    return CachedNetworkImage(
      imageUrl: fullURL,
      placeholder: (context, url) => Center(
        child: SizedBox(
          width: width.toDouble() / 3.5,
          height: height.toDouble() / 3.5,
          child: new PlatformCircularProgressIndicator(),
        ),
      ),
      imageRenderMethodForWeb: ImageRenderMethodForWeb.HttpGet,
      httpHeaders: {
        "X-Imagini-AccessToken": accessToken,
        "X-Imagini-RefreshToken": refreshToken,
      },
      fit: BoxFit.contain,
    );
  }

  Future<QueryResult> me() async {
    QueryResult response = await _client.query(
      QueryOptions(
        document: MeQuery().document,
      )
    );
    return response;
  }

  Future<QueryResult> mediaItems(Page page) async {
    QueryResult response = await _client.query(
      QueryOptions(
        document: MediaItemsQuery().document,
        variables: {
          "page": page,
        },
      )
    );
    return response;
  }

  void dispose() {}
}
