class LoginResponse {
  final String success;
  final String error;
  final String deviceUUID;

  LoginResponse(this.success, this.error, this.deviceUUID);

  LoginResponse.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        error = json['error'],
        deviceUUID = json['deviceUUID'];

  Map<String, dynamic> toJson() =>
    {
      'success': success,
      'error': error,
      'deviceUUID': deviceUUID,
    };
}
