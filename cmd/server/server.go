package server

import (
	"context"
	"net/http"
	"time"

	"github.com/h2non/bimg"
	log "github.com/sirupsen/logrus"

	"reichard.io/imagini/internal/api"
	"reichard.io/imagini/internal/auth"
	"reichard.io/imagini/internal/config"
	"reichard.io/imagini/internal/db"
)

type Server struct {
	API        *api.API
	Auth       *auth.AuthManager
	Config     *config.Config
	Database   *db.DBManager
	httpServer *http.Server
}

func NewServer() *Server {
	c := config.Load()
	db := db.NewMgr(c)
	auth := auth.NewMgr(db, c)
	api := api.NewApi(db, c, auth)

	return &Server{
		API:      api,
		Auth:     auth,
		Config:   c,
		Database: db,
	}
}

func (s *Server) StartServer() {
	listenAddr := (":" + s.Config.ListenPort)

	s.httpServer = &http.Server{
		Handler: s.API.Router,
		Addr:    listenAddr,
	}

	go func() {
		bimg.VipsCacheSetMax(0)
		bimg.VipsCacheSetMaxMem(0)

		err := s.httpServer.ListenAndServe()
		if err != nil {
			log.Error("Error starting server ", err)
			return
		}
	}()
}

func (s *Server) StopServer() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	s.httpServer.Shutdown(ctx)
}
