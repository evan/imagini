package main

import (
	"os"
	"os/signal"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"reichard.io/imagini/cmd/server"

	"github.com/99designs/gqlgen/api"
	"github.com/99designs/gqlgen/codegen/config"
	"reichard.io/imagini/plugin"
)

type UTCFormatter struct {
	log.Formatter
}

func (u UTCFormatter) Format(e *log.Entry) ([]byte, error) {
	e.Time = e.Time.UTC()
	return u.Formatter.Format(e)
}

func main() {
	log.SetFormatter(UTCFormatter{&log.TextFormatter{FullTimestamp: true}})

	app := &cli.App{
		Name:  "Imagini",
		Usage: "A self hosted photo library.",
		Commands: []*cli.Command{
			{
				Name:    "serve",
				Aliases: []string{"s"},
				Usage:   "Start Imagini web server.",
				Action:  cmdServer,
			},
			{
				Name:   "generate",
				Usage:  "generate graphql schema",
				Action: cmdGenerate,
			},
		},
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func cmdServer(ctx *cli.Context) error {
	log.Info("Starting Imagini Server")
	server := server.NewServer()
	server.StartServer()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	server.StopServer()
	os.Exit(0)

	return nil
}

func cmdGenerate(ctx *cli.Context) error {
	log.Info("Generating Imagini Models")
	gqlgenConf, err := config.LoadConfigFromDefaultLocations()
	if err != nil {
		log.Panic("Failed to load config", err.Error())
		os.Exit(2)
	}

	log.Info("Generating Schema...")
	err = api.Generate(gqlgenConf,
		api.AddPlugin(plugin.New()),
	)
	log.Info("Schema Generation Done")
	if err != nil {
		log.Panic(err.Error())
		os.Exit(3)
	}
	os.Exit(0)
	return nil
}
