package db

import (
	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"reichard.io/imagini/graph/model"
)

func (dbm *DBManager) CreateAlbum(album *model.Album) error {
	log.Debug("[db] Creating album: ", album.Name)
	err := dbm.db.Create(album).Error
	return err
}

func (dbm *DBManager) Album(album *model.Album) (int64, error) {
	var count int64
	err := dbm.db.Where(album).First(album).Count(&count).Error
	return count, err
}

func (dbm *DBManager) Albums(userID string, filters *model.AlbumFilter, page *model.Page, order *model.Order) ([]*model.Album, model.PageResponse, error) {
	// Initial User Filter
	tx := dbm.db.Session(&gorm.Session{}).Model(&model.Album{}).Where("user_id == ?", userID)

	// Dynamically Generate Base Query
	tx, pageResponse := dbm.generateBaseQuery(tx, filters, page, order)

	// Acquire Results
	var foundAlbums []*model.Album
	err := tx.Find(&foundAlbums).Error
	return foundAlbums, pageResponse, err
}

func (dbm *DBManager) DeleteAlbum(album *model.Album) error {
	return nil
}
