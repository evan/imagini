package db

import (
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"

	"reichard.io/imagini/graph/model"
)

func (dbm *DBManager) CreateUser(user *model.User) error {
	log.Info("[db] Creating user: ", user.Username)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(*user.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Error(err)
		return err
	}
	stringHashedPassword := string(hashedPassword)
	user.Password = &stringHashedPassword
	return dbm.db.Create(user).Error
}

func (dbm *DBManager) User(user *model.User) (int64, error) {
	var count int64
	err := dbm.db.Where(user).First(user).Count(&count).Error
	return count, err
}

func (dbm *DBManager) Users(filters *model.UserFilter, page *model.Page, order *model.Order) ([]*model.User, model.PageResponse, error) {
	// Initial User Filter
	tx := dbm.db.Session(&gorm.Session{}).Model(&model.Tag{})

	// Dynamically Generate Base Query
	tx, pageResponse := dbm.generateBaseQuery(tx, filters, page, order)

	// Acquire Results
	var foundUsers []*model.User
	err := tx.Find(&foundUsers).Error
	return foundUsers, pageResponse, err
}

func (dbm *DBManager) DeleteUser(user model.User) error {
	return nil
}

func (dbm *DBManager) UpdatePassword(user model.User, pw string) {

}
