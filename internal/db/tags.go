package db

import (
	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"reichard.io/imagini/graph/model"
)

func (dbm *DBManager) CreateTag(tag *model.Tag) error {
	log.Debug("[db] Creating tag: ", tag.Name)
	err := dbm.db.Create(tag).Error
	return err
}

func (dbm *DBManager) Tag(tag *model.Tag) (int64, error) {
	var count int64
	err := dbm.db.Where(tag).First(tag).Count(&count).Error
	return count, err
}

func (dbm *DBManager) Tags(userID string, filters *model.TagFilter, page *model.Page, order *model.Order) ([]*model.Tag, model.PageResponse, error) {
	// Initial User Filter
	tx := dbm.db.Session(&gorm.Session{}).Model(&model.Tag{}).Where("user_id == ?", userID)

	// Dynamically Generate Base Query
	tx, pageResponse := dbm.generateBaseQuery(tx, filters, page, order)

	// Acquire Results
	var foundTags []*model.Tag
	err := tx.Find(&foundTags).Error
	return foundTags, pageResponse, err
}

func (dbm *DBManager) DeleteTag(tag *model.Tag) error {
	return nil
}
