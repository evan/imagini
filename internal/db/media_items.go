package db

import (
	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"reichard.io/imagini/graph/model"
)

func (dbm *DBManager) CreateMediaItem(mediaItem *model.MediaItem) error {
	log.Debug("[db] Creating media item: ", mediaItem.FileName)
	err := dbm.db.Create(mediaItem).Error
	return err
}

func (dbm *DBManager) MediaItem(mediaItem *model.MediaItem) (int64, error) {
	var count int64
	err := dbm.db.Where(mediaItem).First(mediaItem).Count(&count).Error
	return count, err
}

// UserID, Filters, Sort, Page, Delete
func (dbm *DBManager) MediaItems(userID string, filters *model.MediaItemFilter, page *model.Page, order *model.Order) ([]*model.MediaItem, model.PageResponse, error) {
	// Initial User Filter
	tx := dbm.db.Session(&gorm.Session{}).Model(&model.MediaItem{}).Where("user_id == ?", userID)

	// Dynamically Generate Base Query
	tx, pageResponse := dbm.generateBaseQuery(tx, filters, page, order)

	// Acquire Results
	var mediaItems []*model.MediaItem
	err := tx.Find(&mediaItems).Error
	return mediaItems, pageResponse, err
}
