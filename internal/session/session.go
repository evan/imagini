package session

import (
	"sync"
)

// Used to maintain a cache of user specific jwt secrets
// This will prevent DB lookups on every request
// May not actually be needed. Refresh Token is the only
// token that will require proactive DB lookups.
type SessionManager struct {
	mutex  sync.Mutex
	values map[string]string
}

func NewMgr() *SessionManager {
	return &SessionManager{}
}

func (sm *SessionManager) Set(key, value string) {
	sm.mutex.Lock()
	sm.values[key] = value
	sm.mutex.Unlock()
}

func (sm *SessionManager) Get(key string) string {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()
	return sm.values[key]
}

func (sm *SessionManager) Delete(key string) {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()
	_, exists := sm.values[key]
	if !exists {
		return
	}
	delete(sm.values, key)
}
