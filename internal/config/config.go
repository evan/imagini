package config

import (
	"os"
)

type Config struct {
	DBType     string
	DBName     string
	DBPassword string
	DataPath   string
	ConfigPath string
	JWTSecret  string
	ListenPort string
}

func Load() *Config {
	return &Config{
		DBType:     getEnv("DATABASE_TYPE", "SQLite"),
		DBName:     getEnv("DATABASE_NAME", "imagini"),
		DBPassword: getEnv("DATABASE_PASSWORD", ""),
		ConfigPath: getEnv("CONFIG_PATH", "/config"),
		DataPath:   getEnv("DATA_PATH", "/data"),
		JWTSecret:  getEnv("JWT_SECRET", "58b9340c0472cf045db226bc445966524e780cd38bc3dd707afce80c95d4de6f"),
		ListenPort: getEnv("LISTEN_PORT", "8484"),
	}
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
