package api

import (
	"bytes"
	"errors"
	"net/http"
	"os"
	"path"
	"strconv"
	"time"

	"github.com/h2non/bimg"
	log "github.com/sirupsen/logrus"
	"reichard.io/imagini/graph/model"
)

/**
 * Responsible for serving up static images / videos
 **/
func (api *API) mediaHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	if path.Dir(r.URL.Path) != "/media" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	// Acquire Width & Height Parameters
	width, _ := strconv.Atoi(r.URL.Query().Get("width"))

	// Pull out userID
	authContext := r.Context().Value("auth").(*model.AuthContext)
	rawUserID, _ := (*authContext.AccessToken).Get("sub")
	userID := rawUserID.(string)

	// Derive Path
	fileName := path.Base(r.URL.Path)
	folderPath := path.Join("/" + api.Config.DataPath + "/media/" + userID)
	mediaPath := path.Join(folderPath + "/" + fileName)

	// Check if File Exists
	_, err := os.Stat(mediaPath)
	if os.IsNotExist(err) {
		log.Warn("[media] Image does not exist on disk")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	mediaFile, err := resizeAndConvertImageBIMG(mediaPath, width)
	//mediaFile, err := resizeAndConvertImage(mediaPath, width)
	if err != nil {
		log.Warn("[media] Image conversion failed:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// TODO: Cache mediaFile

	http.ServeContent(w, r, fileName, time.Time{}, bytes.NewReader(mediaFile))
}

func resizeAndConvertImageBIMG(path string, desiredWidth int) ([]byte, error) {
	buffer, err := bimg.Read(path)
	if err != nil {
		return nil, errors.New("[media] Unable to read image")
	}
	inputImage := bimg.NewImage(buffer)

	// Return RAW
	if inputImage.Type() == "jpeg" && desiredWidth == 0 {
		return buffer, nil
	}

	imageSize, err := inputImage.Size()

	options := bimg.Options{
		Quality: 100,
		// NoAutoRotate: true,
		Background: bimg.Color{R: 255, G: 255, B: 255},
	}

	// Set dimensions
	if desiredWidth != 0 && imageSize.Width > desiredWidth {
		options.Width = desiredWidth
		options.Quality = 50
	}

	// Convert to JPEG
	if inputImage.Type() != "jpeg" {
		options.Type = bimg.JPEG
	}

	// Process image
	return inputImage.Process(options)
}
