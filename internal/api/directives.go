package api

import (
	"context"
	"errors"

	"github.com/99designs/gqlgen/graphql"
	"reichard.io/imagini/graph/model"
)

/**
 * This is used to validate whether the users role is adequate for the requested resource.
 **/
func (api *API) hasMinRoleDirective(ctx context.Context, obj interface{}, next graphql.Resolver, role model.Role) (res interface{}, err error) {
	authContext := ctx.Value("auth").(*model.AuthContext)
	accessToken, err := api.validateTokens(authContext.AuthResponse, authContext.AuthRequest)
	if err != nil {
		return nil, errors.New("Access Denied")
	}
	authContext.AccessToken = &accessToken

	userRole, ok := accessToken.Get("role")
	if !ok {
		return nil, errors.New("Access Denied")
	}

	if userRole == model.RoleAdmin.String() {
		return next(ctx)
	}

	if userRole == role.String() {
		return next(ctx)
	}

	return nil, errors.New("Role Not Authenticated")
}

/**
 * This is needed but not used. Meta is used for Gorm.
 **/
func (api *API) metaDirective(ctx context.Context, obj interface{}, next graphql.Resolver, gorm *string) (res interface{}, err error) {
	return next(ctx)
}

/**
 * This overrides the response so fields with an @isPrivate directive are always nil.
 **/
func (api *API) isPrivateDirective(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	return nil, errors.New("Private Field")
}
