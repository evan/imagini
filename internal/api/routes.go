package api

import (
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"

	"reichard.io/imagini/graph"
	"reichard.io/imagini/graph/generated"
)

func (api *API) registerRoutes() {
	// Set up Directives
	graphConfig := generated.Config{
		Resolvers: &graph.Resolver{
			DB:     api.DB,
			Auth:   api.Auth,
			Config: api.Config,
		},
		Directives: generated.DirectiveRoot{
			Meta:       api.metaDirective,
			IsPrivate:  api.isPrivateDirective,
			HasMinRole: api.hasMinRoleDirective,
		},
	}
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(graphConfig))

	// Handle GraphQL
	api.Router.Handle("/playground", playground.Handler("GraphQL playground", "/query"))
	api.Router.Handle("/query", api.queryMiddleware(srv))

	// Handle Resource Route
	api.Router.HandleFunc("/media/", multipleMiddleware(
		api.mediaHandler,
		api.authMiddleware,
	))
}
