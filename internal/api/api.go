package api

import (
    "net/http"

    "reichard.io/imagini/internal/db"
    "reichard.io/imagini/internal/auth"
    "reichard.io/imagini/internal/config"
)

type API struct {
    Router  *http.ServeMux
    Config  *config.Config
    Auth    *auth.AuthManager
    DB      *db.DBManager
}

func NewApi(db *db.DBManager, c *config.Config, auth *auth.AuthManager) *API {
    api := &API{
        Router:  http.NewServeMux(),
        Config:  c,
        Auth:    auth,
        DB:      db,
    }
    api.registerRoutes()
    return api
}
