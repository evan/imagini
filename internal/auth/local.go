package auth

import (
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	"reichard.io/imagini/graph/model"
)

func authenticateLocalUser(user model.User, pw string) bool {
	bPassword := []byte(pw)
	err := bcrypt.CompareHashAndPassword([]byte(*user.Password), bPassword)
	if err == nil {
		log.Info("[auth] Authentication successfull: ", user.Username)
		return true
	}
	log.Warn("[auth] Authentication failed: ", user.Username)
	return false
}
